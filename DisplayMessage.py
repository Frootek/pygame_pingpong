import pygame

TEXT_SIZE = 100

def text_objects(text, font, color):
    textSurface = font.render(text, True, color)
    return textSurface, textSurface.get_rect()

def message_display(screen,text,color,width,height, text_size=TEXT_SIZE):

    largeText = pygame.font.SysFont("comicsansms", int(text_size))
    TextSurf, TextRect = text_objects(text, largeText,color)
    TextRect.center = (width/2), (height/2)
    screen.blit(TextSurf, TextRect)
    pygame.display.update()