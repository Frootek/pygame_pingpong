
import pygame_gui
import pygame
from DisplayMessage import message_display
WHITE = (255,255,255)

BUTTON_WIDTH = 300
BUTTON_HEIGHT = 50
BUTTON_SIZE = (BUTTON_WIDTH, BUTTON_HEIGHT)


class AboutGui(pygame_gui.UIManager):
    def __init__(self, screen, window_resolution):
        super().__init__(window_resolution)

        self.screen = screen

        self.screen_width, self.screen_height = window_resolution

        self.center_x = self.screen_width / 2


        #NOTE : 756px is tested height for y axis of gui, so we need to scale for other resolutions

        self.scale_y = self.screen_height * 1/756

        message_display(self.screen, "PingPong by Frootek",WHITE, self.screen_width, 200 * self.scale_y)

        message_display(self.screen, "My name is Marin Juric and I am currently  a student at FER, Zagreb.",WHITE, self.screen_width, 600 * self.scale_y, text_size=int(25 * self.scale_y))

        message_display(self.screen, "In my attempt to master python I created this small game as a practice in pythons module pygame,",WHITE, self.screen_width, 700 * self.scale_y, text_size=25 * self.scale_y)

        message_display(self.screen, "pygame_gui and design patterns.",WHITE, self.screen_width, 800 * self.scale_y, text_size=25 * self.scale_y)


        message_display(self.screen, "Right player is controlled by UP and DOWN keyboard buttons.",WHITE, self.screen_width, 900 * self.scale_y, text_size=25 * self.scale_y)

        message_display(self.screen, "Left player is controlled by W  ans S keyboard buttons.",WHITE, self.screen_width, 1000 * self.scale_y, text_size=25 * self.scale_y)

        
        self.main_menu = pygame_gui.elements.UIButton(relative_rect=pygame.Rect((self.center_x - BUTTON_WIDTH/2, 650 * self.scale_y ), BUTTON_SIZE),
        text="Back to main menu",manager=self)    


    #return to main menu btn
    def process_events(self, event):
        if event.type == pygame.USEREVENT:
            if event.user_type == pygame_gui.UI_BUTTON_PRESSED:
                if event.ui_element == self.main_menu:
                    return "MAIN_MENU"

                    

        super().process_events(event)