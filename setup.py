import cx_Freeze
import sys


base = None


executables = [cx_Freeze.Executable("App.py", base = "Win32GUI")]
cx_Freeze.setup(
    name="PingPong by Frootek",
    options={"build_exe": {"packages":["pygame", "pygame_gui"
                            ,"os", "time", "sys", "random"]}},
    version = "1.0",
    executables = executables

    )