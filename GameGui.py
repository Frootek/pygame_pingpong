import pygame
import pygame_gui
from ScoreLabel import ScoreLabel

TOP_LEFT_BTN_X = 20 
TOP_LEFT_BTN_Y = 20

BUTTON_WIDTH = 100
BUTTON_HEIGHT = 50
BUTTON_SIZE = (BUTTON_WIDTH,BUTTON_HEIGHT)
BUTTON_PAD = 5

LABEL_WIDTH  = 100
LABEL_HEIGHT = 40
LABEL_SIZE = (LABEL_WIDTH, LABEL_HEIGHT)

WIN_SCORE = 10

WHITE = (255,255,255)

upper_right_anchor={'left': 'right',
        'right': 'right',
        'top': 'top',
        'bottom': 'top'}


GUI_HEIGHT_NORMAL = 180

""" 
    GUI THAT HOLDS BUTTONS AND SCORE LABEL
"""

class GameGui(pygame_gui.UIManager):
    def __init__(self, screen, window_resolution, gui_height):
        super().__init__(window_resolution)

        self.screen = screen
        self.screen_width, self.screen_height = window_resolution
        self.gui_height = gui_height

        """
            FIRST we need to scale our GUI according to gui_height property
        """

        self.y_multiplier = gui_height / GUI_HEIGHT_NORMAL

        self.button_size = (BUTTON_WIDTH, BUTTON_HEIGHT * self.y_multiplier )
        self.label_size =  (LABEL_WIDTH, LABEL_HEIGHT * self.y_multiplier )


        #TODO bind each button with proper functionality


        self.main_menu_btn = pygame_gui.elements.UIButton(relative_rect=pygame.Rect((TOP_LEFT_BTN_X, TOP_LEFT_BTN_Y * self.y_multiplier), self.button_size),
                                                    text="Main Menu",manager=self)



        self.new_game_btn = pygame_gui.elements.UIButton(relative_rect=pygame.Rect((-TOP_LEFT_BTN_X-BUTTON_WIDTH, TOP_LEFT_BTN_Y * self.y_multiplier), self.button_size),
                                                    text="New Game",manager=self, anchors=upper_right_anchor)
        
    
        self.pause_btn = pygame_gui.elements.UIButton(relative_rect=pygame.Rect((-TOP_LEFT_BTN_X-2*BUTTON_WIDTH-BUTTON_PAD, TOP_LEFT_BTN_Y * self.y_multiplier), self.button_size),
                                                    text="Pause",manager=self, anchors=upper_right_anchor)

        
        #define labels for player names and their scores 

        self.left_player_label  = pygame_gui.elements.UILabel(relative_rect=pygame.Rect((TOP_LEFT_BTN_X, (2* TOP_LEFT_BTN_Y + BUTTON_HEIGHT) * self.y_multiplier), self.label_size),
                                                        text="PLAYER_1", manager=self)

        
        self.right_player_label  = pygame_gui.elements.UILabel(relative_rect=pygame.Rect((-TOP_LEFT_BTN_X-BUTTON_WIDTH, (2* TOP_LEFT_BTN_Y + BUTTON_HEIGHT) * self.y_multiplier ), self.label_size),
                                                text="PLAYER_2", manager=self,anchors=upper_right_anchor)

        
        #INIT score_label

        score_width = self.screen_width - 2 * 20 - 2 * LABEL_WIDTH - 2 * 20
        score_height = LABEL_HEIGHT * self.y_multiplier
        top_x = 20 + LABEL_WIDTH + 20
        top_y = (2* TOP_LEFT_BTN_Y + BUTTON_HEIGHT) * self.y_multiplier

        self.score_label = ScoreLabel(screen, score_width, score_height, top_x, top_y, WIN_SCORE)

       
        
        ##draw a line at gui_height and window height
        pygame.draw.line(screen, WHITE,(0, gui_height),(self.screen_width, gui_height),  1)
        pygame.draw.line(screen, WHITE,(0, self.screen_height-1),(self.screen_width, self.screen_height-1), 1)


    def process_events(self, event):
        if event.type == pygame.USEREVENT:
            if event.user_type == pygame_gui.UI_BUTTON_PRESSED:
                if event.ui_element == self.main_menu_btn :
                    return "MAIN_MENU"
                if event.ui_element == self.pause_btn :
                    return "PAUSE"
                if event.ui_element == self.new_game_btn :
                    return "NEW_GAME"

        super().process_events(event)

    def reset_score_label(self):
        self.score_label.reset_score_label()
        