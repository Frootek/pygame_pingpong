

from math import cos, sin, tan, radians

"""
    STEP = 6, every 6 bounces speed up the ball movement by SPEED_UP_NUM
"""


STEP = 6
SPEED_UP_NUM = 0.5
MAX_SPEED = 3.5

class Ball(object):
    def __init__(self, initial_x, initial_y, radius, alpha_angle, screen_height_y1, screen_height_y2, player_one, player_two):

        self.initial_x = initial_x
        self.initial_y = initial_y
        self.initial_angle = alpha_angle

        self.x = initial_x
        self.y = initial_y
        self.radius = radius
        self.angle = alpha_angle

        self.screen_height_y1 = screen_height_y1
        self.screen_height_y2 = screen_height_y2

        self.player_one = player_one
        self.player_two = player_two

        self.bounces = 0
        self.speed = 1

    """
        Movement of the ball will be defined by the angle it is currently moving.

    """   
    def move(self):
        self.y += sin(radians(self.angle)) * self.speed
        self.x += cos(radians(self.angle)) * self.speed
        self.collision()


    def collision(self):
        #WALL REFLECTION
        if((self.y + self.radius) >= self.screen_height_y2 or (self.y-self.radius) <= self.screen_height_y1):
            self.wall_reflection()

        ##check collision between ball and left and right player
    
        if(self.x <= (self.player_one.x + 25) and self.x > self.player_one.x + 20):
            if( self.player_one.y <= self.y <= (self.player_one.y + self.player_one.board_length )):
                self.board_reflection()

                """
                    Every STEP increase the speed, and every 2*STEP shorthen the board also,
                    muahahaha
                """
                self.bounces += 1
                if(self.bounces % STEP == 0):
                    self.speed += SPEED_UP_NUM

                if(self.bounces % (2*STEP) == 0):
                    self.player_one.shorten_board()
                    self.player_two.shorten_board()

        if(self.x >= self.player_two.x and self.x < self.player_two.x + 5 ):
            if( self.player_two.y <= self.y <= (self.player_two.y + self.player_two.board_length )):
                self.board_reflection()

                self.bounces += 1
                if(self.bounces % STEP == 0 and self.speed <= MAX_SPEED):
                    self.speed += SPEED_UP_NUM

                if(self.bounces % (2*STEP) == 0 and self.speed <= MAX_SPEED):
                    self.player_one.shorten_board()
                    self.player_two.shorten_board()


    def wall_reflection(self):
        self.angle = abs(self.angle % 360)
        self.angle = 360 - self.angle
    
    def board_reflection(self):
        self.angle = abs(self.angle % 360)
        self.angle = 360 - self.angle
        self.angle = 180 + self.angle

    
    def reset(self):
        self.initial_angle = 180-self.initial_angle
        self.angle = self.initial_angle
        self.x = self.initial_x
        self.y = self.initial_y
        self.bounces = 0
        self.speed =  1

    def reset_game(self):
        self.reset()




    

        