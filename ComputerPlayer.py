
from Player import Player


"""
    AI for this game is pretty simple
    We inherit from player class and give AI one aditional information, position of the ball.
    Then we overide move method according to ball position. Kinda op actually.
"""

class ComputerPlayer(Player):
    def __init__(self,initial_x ,initial_board_length, side, screen_height_y1, screen_height_y2):
        super().__init__(initial_x ,initial_board_length, side, screen_height_y1, screen_height_y2)
        self.ball = None


    
    def move(self, keys):
        if ((self.y + self.board_length/2) < self.ball.y):
            if(abs(self.ball.x-self.x)<300):
                super().move_down()


        if ((self.y + self.board_length/2) > self.ball.y):
            if(abs(self.ball.x-self.x)<300):
                super().move_up()
    
    def set_ball(self, ball):
        self.ball = ball