import pygame

CIRCLE_PAD = 5
SCORE_PAD = 20
MAX_SCORE = 5

WHITE  = (255,255,255)
BLACK  = (0, 0, 0)

LINE_THICKNESS = 1

class ScoreLabel(object):
    def __init__(self, screen, label_width, label_height, top_x, top_y, WIN_SCORE = MAX_SCORE):
        super().__init__()
        self.screen = screen
        self.label_width = label_width
        self.label_height = label_height
        self.top_x = top_x
        self.top_y = top_y
        self.left_player_score = 0
        self.right_player_score = 0

        self.win_score = WIN_SCORE

        self.x_center = label_width / 2

        self.cell_width = (self.x_center - self.top_x- SCORE_PAD/2) / self.win_score
        self.circle_y = top_y + label_height / 2
        self.circle_radius = self.cell_width / 2 - CIRCLE_PAD

        self.first_x_left = self.top_x + CIRCLE_PAD + self.circle_radius
        self.first_x_right = self.top_x + self.label_width - CIRCLE_PAD - self.circle_radius

        self.draw_empty_circles()
    
    #for init and reset
    def draw_empty_circles(self):
        #draw circles for left player

        x = self.first_x_left  
        for i in range (0, self.win_score):
            # First draw black circle then white circle !!!!
            pygame.draw.circle(self.screen, BLACK, (int(x), int(self.circle_y)), int(self.circle_radius))
            pygame.draw.circle(self.screen, WHITE, (int(x), int(self.circle_y)), int(self.circle_radius),LINE_THICKNESS)
            x = x + self.cell_width

        #draw circles for right player

        x = self.first_x_right
        for i in range (0, self.win_score):
            pygame.draw.circle(self.screen, BLACK, (int(x), int(self.circle_y)), int(self.circle_radius))
            pygame.draw.circle(self.screen, WHITE, (int(x), int(self.circle_y)), int(self.circle_radius), LINE_THICKNESS)
            x = x - self.cell_width


    def fill_circle(self, x):
        pygame.draw.circle(self.screen, WHITE, (int(x), int(self.circle_y)), int(self.circle_radius))


    def update_score_by_one(self, player):
        if (player == "L"):
            self.left_player_score += 1
            if(self.left_player_score <= self.win_score):
                x = self.first_x_left + (self.left_player_score-1) * self.cell_width
                self.fill_circle(int(x))

        if (player == "R"):
            self.right_player_score += 1
            if(self.right_player_score <= self.win_score):
                x = self.first_x_right - (self.right_player_score-1) * self.cell_width
                self.fill_circle(int(x))
    
    def reset_score_label(self):
        self.left_player_score = 0
        self.right_player_score = 0
        self.draw_empty_circles()



    def check_winner(self):
        if(self.left_player_score >=  self.win_score):
            return "L"
        if(self.right_player_score >=  self.win_score):
            return "R"

        return False
