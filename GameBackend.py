import pygame

from time import sleep
from Player import Player
from Ball import Ball
from DisplayMessage import message_display

WHITE  = (255,255,255)
BLACK  = (0,0,0)
RED  = (255,0,0)
GREEN = (0, 255, 0)


BOARD_WIDTH = 25


BALL_RADIUS = 20


class GameBackend(object):

    def __init__(self, screen, screen_width, screen_height_y1, screen_height_y2, score_label, player_one, player_two):

        self.screen = screen
        self.screen_width = screen_width
        self.screen_height_y1 = screen_height_y1
        self.screen_height_y2 = screen_height_y2

        self.y_center = ((self.screen_height_y2 - self.screen_height_y1) / 2) + screen_height_y1
        

        # Create two players

        """ 
        self.player_one = Player(L_PLAYER_INITIAL_X, BOARD_LENGTH,"L",self.screen_height_y1 + 1 ,self.screen_height_y2 - 1)

        self.player_two = Player(self.screen_width- 20 - BOARD_WIDTH, BOARD_LENGTH, "R", self.screen_height_y1 + 1 ,self.screen_height_y2 - 1)
        """
        self.ball = Ball(self.screen_width/2, self.y_center,BALL_RADIUS, -15, self.screen_height_y1 + 1  ,self.screen_height_y2-1 , player_one, player_two)

        self.player_one = player_one
        self.player_two = player_two

        self.score_label = score_label


    """
        Basicaly main loop, we have to erase both players, move them according to  the event if event is a valid move, then draw them again.
        Delete and draw the the "ball" again.
        Returns True if game is won by one side, False otherwise.

    """
    #NOTE when game is won return True
    def process_event(self):

        keys = pygame.key.get_pressed()

        # DELETE PREVIOUS POSITION OF PLAYERS AND BALL
        self.draw_players(BLACK)
        self.draw_ball(BLACK)
    
        self.player_one.move(keys)
        self.player_two.move(keys)
        self.ball.move()


        self.draw_ball(WHITE)
        self.draw_players(WHITE)

        #check if ball is out of bounds if it is start new round
        if(self.check_ball_out()):
            self.new_round()

        player_won = self.check_win()

        if (player_won != False):
            self.declare_winner(player_won)
            return True
        
            
        pygame.display.update()

        return False

    def check_win(self):
        return self.score_label.check_winner()



    def declare_winner(self, side):
        text = ""

        if  side == "L":
            text = "Player_1 wins!"
        if  side == "R":
            text = "Player_2 wins!"
        
        message_display(self.screen, text , GREEN, self.screen_width, self.screen_height_y2 * 4/3, 50 )

        sleep(3)
    
        pygame.draw.rect(self.screen , BLACK, (0, self.screen_height_y2 * 1/3 + 5, self.screen_width, self.screen_height_y2 * 2/3))
    


    def draw_players(self, COLOR):
        pygame.draw.rect(self.screen , COLOR, (self.player_one.get_x(), self.player_one.get_y(), BOARD_WIDTH, self.player_one.get_board_length()))
        pygame.draw.rect(self.screen , COLOR, (self.player_two.get_x(), self.player_two.get_y(), BOARD_WIDTH, self.player_one.get_board_length()))

    def draw_ball(self, COLOR):
        pygame.draw.circle(self.screen, COLOR, (int(self.ball.x), int(self.ball.y)), self.ball.radius)

    def check_ball_out(self):
        if(self.ball.x < 0):
            self.player_two.update_score()
            self.score_label.update_score_by_one("R")
            return True
        if(self.ball.x > self.screen_width):
            self.player_one.update_score()
            self.score_label.update_score_by_one("L")
            return True
        
        return False

    def new_round(self):
        self.draw_players(BLACK)
        self.draw_ball(BLACK)

        self.player_one.reset()
        self.player_two.reset()
        self.ball.reset()

    def get_players(self):
        return (self.player_one, self.player_two)

    def reset_game(self):
        self.draw_players(BLACK)
        self.draw_ball(BLACK)
        self.player_one.new_game()
        self.player_two.new_game()
        self.ball.reset()
        self.score_label.reset_score_label()