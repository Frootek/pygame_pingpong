import pygame
import pygame_gui

import os
os.environ['SDL_VIDEO_CENTERED'] = '1'

from GameBackend import GameBackend
from GameGui import GameGui
from DisplayMessage import message_display

from Player import Player
from ComputerPlayer import ComputerPlayer

BLACK = (0,0,0)

RED = (255,0,0)

WHITE = (255,255,255)

"""
    Class that encapsulates local game.
    After initialization runs game_loop

    TYPE ENUM
    LOCAL - local game
    PLAYER_VS_COMP - PLAYER VS COMPUTER
    COMP_VS_COMP - COMPUTER VS COMPUTER

"""

BOARD_LENGTH = 150
BOARD_WIDTH = 25

L_PLAYER_INITIAL_X = 20

class Game(object):
    def __init__(self,display, window_resolution, TYPE, MAIN_MENU = None,):
        pygame.init()

        self.main_menu = MAIN_MENU

        info = pygame.display.Info()

        self.width = info.current_w
        self.height = info.current_h 
        
        self.screen = display

        self.WINDOW_SIZE = (int(self.width), int(self.height))


        self.clock = pygame.time.Clock()
        
        self.manager = GameGui(self.screen, self.WINDOW_SIZE, self.height * 1/3)

        self.screen_height_y1 =  self.height * 1/3 + 1

        self.screen_height_y2 = self.height-1

        #DIFERENT COMBINATION OF PLAYERS DEPENDING ON BUTTON PRESSED

        if (TYPE ==  "LOCAL"):
            self.player_one = Player(L_PLAYER_INITIAL_X, BOARD_LENGTH,"L",
                                        self.screen_height_y1 + 1 ,self.screen_height_y2 - 1)

            self.player_two = Player(self.width- 20 - BOARD_WIDTH, BOARD_LENGTH, "R", 
                                    self.screen_height_y1 + 1 ,self.screen_height_y2 - 1)
        if (TYPE ==  "COMP_VS_COMP"):
            self.player_one = ComputerPlayer(L_PLAYER_INITIAL_X, BOARD_LENGTH,"L",
                                        self.screen_height_y1 + 1 ,self.screen_height_y2 - 1)

            self.player_two = ComputerPlayer(self.width- 20 - BOARD_WIDTH, BOARD_LENGTH, "R", 
                                    self.screen_height_y1 + 1 ,self.screen_height_y2 - 1)

        if (TYPE ==  "PLAYER_VS_COMP"):
            self.player_one = ComputerPlayer(L_PLAYER_INITIAL_X, BOARD_LENGTH,"L",
                                        self.screen_height_y1 + 1 ,self.screen_height_y2 - 1)

            self.player_two = Player(self.width- 20 - BOARD_WIDTH, BOARD_LENGTH, "R", 
                                    self.screen_height_y1 + 1 ,self.screen_height_y2 - 1)
            


        self.game = GameBackend(self.screen, self.width, self.screen_height_y1, self.screen_height_y2, self.manager.score_label,
        self.player_one, self.player_two )

        if(TYPE == "COMP_VS_COMP"):
            self.player_one.set_ball(self.game.ball)
            self.player_two.set_ball(self.game.ball)
        if (TYPE == "PLAYER_VS_COMP"):
            self.player_one.set_ball(self.game.ball)





        self.btn_methods = {
            "MAIN_MENU" : self.start_main_menu,
            "PAUSE" : self.pause,
            "NEW_GAME" :  self.new_game
        }

        self.fps = 80
        self.game_loop()


    

    def game_loop(self):
        running = True


        function = None

        time_delta = self.clock.tick(self.fps)/1000.00
        while running:
            # event handling, gets all event from the event queue
            for event in pygame.event.get():
                # only do something if the event is of type QUIT
                if event.type == pygame.QUIT:
                    # change the value to False, to exit the main loop
                    running = False
            
                answer = self.manager.process_events(event)

                if (answer != None):

                    if (answer == "PAUSE"):
                        self.pause()                    
                    else:
                        function = self.btn_methods[answer]
                        running = False

            #if game.process_event() returns True we should start new game
            if(self.game.process_event()):
                function = self.new_game
                running = False

            self.manager.update(time_delta)
            self.manager.draw_ui(self.screen)

        if(function != None):
            function()



    def start_main_menu(self):
        if (self.main_menu != None):
            self.manager.clear_and_reset()
            self.screen.fill(BLACK)
            self.main_menu.start_main_menu()
    
    def pause(self):

        pause = True
        
        message_display(self.screen,"Paused", WHITE, self.width, self.height * 4/3, 200 )

        while pause:
            for event in pygame.event.get():

                if event.type == pygame.QUIT:
                    pygame.quit()
                keys = pygame.key.get_pressed()
                if keys[pygame.K_SPACE] or keys[pygame.K_KP_ENTER]:
                    pause = False

            
        ##fill in black screen so the Pause message is not displayed anymore

        pygame.draw.rect(self.screen , BLACK, (0, self.height * 1/3 + 1, self.width, self.height * 2/3))

    
    def new_game(self):
        self.game.reset_game()    
        self.game_loop()


#for testing purposes
if __name__.endswith('__main__'):
    app = Game()
