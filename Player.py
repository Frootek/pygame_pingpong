import pygame


"""
    Each player is represented by a board.
    We need board position y which will change as the player moves.
    We need board length which will shrink as round goes on.
    We need player score.

"""
MOVE_DISTANCE = 3.5
BOARD_CHOP  = 5
BOARD_MIN = 50

class Player(object):
    def __init__(self,initial_x ,initial_board_length, side, screen_height_y1, screen_height_y2):

        self.upper_limit = screen_height_y1
        self.lower_limit = screen_height_y2

        self.side = side

        self.observers = []

        self.initial_x = initial_x
        self.initial_y = ((self.lower_limit - self.upper_limit) / 2 + self.upper_limit) - initial_board_length / 2


        self.initial_board_length = initial_board_length

        self.y = self.initial_y
        self.x = initial_x
        
        self.board_length = initial_board_length
        self.player_score = 0

        self.key_movement = {}

        if (side == "L"):
            self.key_movement = {
                pygame.K_w : self.move_up,
                pygame.K_s : self.move_down
            }

        if (side == "R"):
            self.key_movement = {
                pygame.K_UP : self.move_up,
                pygame.K_DOWN: self.move_down
            }

    def move(self, keys):
        
        for key in self.key_movement:
            if keys[key]:
                action = self.key_movement[key]
                action()


    def move_up(self):
        
        if((self.y - MOVE_DISTANCE) < self.upper_limit):
            return
        
        self.y -= MOVE_DISTANCE
    
    def move_down(self):
        if ( (self.y + self.board_length+MOVE_DISTANCE) > self.lower_limit):
            return

        self.y += MOVE_DISTANCE
    
    def shorten_board(self):
        if (self.board_length < BOARD_MIN):
            return
        self.board_length -= BOARD_CHOP

    def get_x(self):
        return self.x

    def get_y(self):
        return self.y

    def get_board_length(self):

        return self.board_length


    def set_y(self, y):
        self.y = y


    def update_score(self):
        self.player_score += 1
            
    def reset(self):
        self.y = self.initial_y
        self.x = self.initial_x
        self.board_length = self.initial_board_length
    
    def new_game(self):
        self.reset()
        self.player_score = 0
