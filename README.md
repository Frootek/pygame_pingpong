# PygamePingPong

To flex my mad python skills I coded one of the oldest computer games :).
You can play local PvP or play against a computer.
I also added mode where two computers play against each other and it is really fun to watch xD.

## Usage

Clone the repository.

If you don't have python installed on your operating system and you are running a Windows Os,
you can simply go inside /build/exe.win-amd64-3.8 and run App.exe.

If you have python you can also run following command 

```bash
python App.py
```

Have fun!

## The game


![Image1](images/pingpong_1.JPG)
![Image1](images/pingpong_2.JPG)