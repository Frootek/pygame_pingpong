
import pygame, pygame_gui
from DisplayMessage import message_display


BUTTON_WIDTH = 300

FIRST_BUTTON_Y = 250
BTN_PAD = 50


WHITE = (255,255,255)

class MainMenuGui(pygame_gui.UIManager):
    def __init__(self, screen, window_resolution):
        super().__init__(window_resolution)

        self.screen = screen
        self.screen_width, self.screen_height = window_resolution

        self.center_x = self.screen_width/2

        self.first_line  = "PingPong by Frootek"

        self.scale_y = self.screen_height * 1/756

        self.BUTTON_HEIGHT = 50  * self.scale_y

        self.BUTTON_SIZE = (BUTTON_WIDTH, self.BUTTON_HEIGHT)

        message_display(self.screen, "PingPong by Frootek",WHITE, self.screen_width, 200 * self.scale_y)



        self.comp_vs_comp = pygame_gui.elements.UIButton(relative_rect=pygame.Rect((self.center_x - BUTTON_WIDTH/2, FIRST_BUTTON_Y * self.scale_y), self.BUTTON_SIZE),
        text="Computer vs Computer",manager=self)

        self.player_vs_comp = pygame_gui.elements.UIButton(relative_rect=pygame.Rect((self.center_x - BUTTON_WIDTH/2, (FIRST_BUTTON_Y + self.BUTTON_HEIGHT + BTN_PAD) * self.scale_y), self.BUTTON_SIZE),
        text="Player VS Computer",manager=self)


        self.local_game_btn = pygame_gui.elements.UIButton(relative_rect=pygame.Rect((self.center_x - BUTTON_WIDTH/2, (FIRST_BUTTON_Y + 2 * (self.BUTTON_HEIGHT + BTN_PAD)) * self.scale_y), self.BUTTON_SIZE),
        text="Player vs Player",manager=self)


        self.about_btn = pygame_gui.elements.UIButton(relative_rect=pygame.Rect((self.center_x - BUTTON_WIDTH/2, (FIRST_BUTTON_Y + 3 * (self.BUTTON_HEIGHT + BTN_PAD)) * self.scale_y ), self.BUTTON_SIZE),
        text="About",manager=self)


    #process events for Main Menu btns
    def process_events(self, event):
        if event.type == pygame.USEREVENT:
            if event.user_type == pygame_gui.UI_BUTTON_PRESSED:
                if event.ui_element == self.local_game_btn:
                    return "LOCAL_GAME"
                if event.ui_element == self.about_btn:
                    return "ABOUT"
                if event.ui_element == self.comp_vs_comp:
                    return "COMP_VS_COMP"
                if event.ui_element == self.player_vs_comp:
                    return "PLAYER_VS_COMP"      
                    

        super().process_events(event)


