import pygame
import pygame_gui

import sys

from MainMenuGui import MainMenuGui

from Game import Game

from About import AboutGui

import os
os.environ['SDL_VIDEO_CENTERED'] = '1'

BLACK = (0, 0, 0)


"""
    Entry point of application.
    Used for switching between different screens (Online, Create, Local, About).
    Also initializes pygame and sets screen_width and screen_height which is used throught application.
"""

class App(object):
    def __init__(self):
        super().__init__()
        pygame.init()

        info = pygame.display.Info()

        self.width = info.current_w * 0.6
        self.height = info.current_h  * 0.7


        self.WINDOW_SIZE = (int(self.width), int(self.height))

        self.screen = pygame.display.set_mode(self.WINDOW_SIZE)
        pygame.display.set_caption("PingPong by Frootek")

        self.clock = pygame.time.Clock()
        self.fps = 60

        #First screen to display is Main Menu
        self.manager = MainMenuGui(self.screen,self.WINDOW_SIZE)

        self.btn_methods = {
            "MAIN_MENU" : self.start_main_menu,
            "LOCAL_GAME" : self.start_local_game,
            "PLAYER_VS_COMP" : self.start_player_vs_comp,
            "COMP_VS_COMP" : self.start_comp_vs_comp,
            "ABOUT" : self.about_page
        }

        self.main_menu_loop()


    

    def main_menu_loop(self):
        running = True

        function = None


        time_delta = self.clock.tick(self.fps)/1000.00
        while running:
            # event handling, gets all event from the event queue
            for event in pygame.event.get():
                # only do something if the event is of type QUIT
                if event.type == pygame.QUIT:
                    # change the value to False, to exit the main loop
                    running = False
            
                answer = self.manager.process_events(event)

                if (answer != None):
                    function = self.btn_methods[answer]
                    running = False


            self.manager.update(time_delta)
            self.manager.draw_ui(self.screen)
            pygame.display.update()

        self.manager.clear_and_reset()
        
        self.screen.fill(BLACK)

        if(function != None):
            function()
    
    def start_main_menu(self):
        self.manager = MainMenuGui(self.screen, self.WINDOW_SIZE)
        self.main_menu_loop()


    def start_local_game(self):
        game = Game(self.screen, self.WINDOW_SIZE,"LOCAL", self)

    def start_comp_vs_comp(self):
        game = Game(self.screen, self.WINDOW_SIZE,"COMP_VS_COMP", self)

    def start_player_vs_comp(self):
        game = Game(self.screen, self.WINDOW_SIZE,"PLAYER_VS_COMP", self)


    def about_page(self):
        self.manager = AboutGui(self.screen, self.WINDOW_SIZE)
        self.main_menu_loop()

if __name__.endswith('__main__'):
    app = App()

